package bhb.user.config;

import filter.LoginFilter;
import filter.LogoutFilter;
import listener.LogoutListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.session.SessionRepository;
import org.springframework.session.events.AbstractSessionEvent;
import org.springframework.session.web.http.SessionEventHttpSessionListenerAdapter;
import org.springframework.web.client.RestTemplate;
import session.SessionMappingStorage;
import session.SmartContainer;
import session.redis.RedisSessionMappingStorage;

import javax.servlet.http.HttpSessionListener;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class SmartSsoConfig {

	@Value("${sso.server.url}")
	private String serverUrl;
	@Value("${sso.app.id}")
	private String appId;
	@Value("${sso.app.secret}")
	private String appSecret;
	@Value("${sso.server.loginPageUrl}")
	private String loginPageUrl;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private SessionRepository<?> sessionRepository;

	@Autowired
	private StringRedisTemplate redisTemplate;


	/**
	 * 分布式redis方式注册单点登出Listener
	 *
	 * 注：
	 * 1.需注入RedisSessionMappingStorage
	 * 2.需要使用Spring方式注入LogoutListener，使用ServletListenerRegistrationBean方式不生效
	 */
	@Autowired
	private SessionMappingStorage sessionMappingStorage;

	@Bean
	public SessionMappingStorage sessionMappingStorage() {
		RedisSessionMappingStorage redisSessionMappingStorage = new RedisSessionMappingStorage();
		redisSessionMappingStorage.setRedisTemplate(redisTemplate);
		redisSessionMappingStorage.setSessionRepository(sessionRepository);
		return redisSessionMappingStorage;
	}

	@Bean
	public ApplicationListener<AbstractSessionEvent> LogoutListener() {
		List<HttpSessionListener> httpSessionListeners = new ArrayList<>();
		LogoutListener logoutListener = new LogoutListener();
		logoutListener.setSessionMappingStorage(sessionMappingStorage);
		httpSessionListeners.add(logoutListener);
		return new SessionEventHttpSessionListenerAdapter(httpSessionListeners);
	}

	/**
	 * 单点登录Filter容器
	 *
	 * @return
	 */
	@Bean
	public FilterRegistrationBean<SmartContainer> smartContainer() {
		SmartContainer smartContainer = new SmartContainer();
		smartContainer.setServerUrl(serverUrl);
		smartContainer.setAppId(appId);
		smartContainer.setAppSecret(appSecret);
		smartContainer.setLoginPageUrl(loginPageUrl);
		smartContainer.setRestTemplate(restTemplate);

		// 忽略拦截URL,多个逗号分隔
		String swagger = "/v2/api-docs,/webjars/*,/swagger-resources/*,/swagger-ui.html";
		smartContainer.setExcludeUrls("/user/getUserByAccount,/user/buildUser," + swagger);

		smartContainer.setFilters(new LogoutFilter(), new LoginFilter());

		FilterRegistrationBean<SmartContainer> registration = new FilterRegistrationBean<>();
		registration.setFilter(smartContainer);
		registration.addUrlPatterns("/*");
		registration.setOrder(1);
		registration.setName("smartContainer");
		return registration;
	}
}
