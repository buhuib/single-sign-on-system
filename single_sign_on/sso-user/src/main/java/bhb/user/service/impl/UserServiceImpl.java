package bhb.user.service.impl;

import bhb.user.dao.UserDao;
import bhb.user.service.UserService;
import bhb.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public User getUserByAccount(String account) {
        return userDao.getUserByAccount(account);
    }


    @Override
    public User getUserByKey(String key) {
        return userDao.getUserByKey(key);
    }

}
