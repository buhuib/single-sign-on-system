package bhb.user.service;

import bhb.model.User;

public interface UserService {


    public User getUserByAccount(String account);

    public User getUserByKey(String account);




}
