package bhb.user.dao;

import bhb.model.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDao {

    User getUserByAccount(String account);

    User getUserByKey(String userKey);

}
