package bhb.user.controller;


import bhb.user.service.UserService;
import io.swagger.annotations.ApiOperation;
import bhb.model.ResponseBean;
import bhb.model.ResultCode;
import bhb.model.SsoUser;
import bhb.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;
import util.SessionUtils;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiIgnore
    @GetMapping("/getUserByAccount")
    public ResponseBean<User> getUserByAccount(
            @RequestParam String account) {
        User userByAccount = userService.getUserByAccount(account);
        return ResponseBean.build(ResultCode.SUCCESS, userByAccount);
    }

    @ApiIgnore
    @GetMapping("/getUserById")
    public User getUserById(
            @RequestParam String account) {
        return userService.getUserByAccount(account);
    }

    @ApiOperation("获取当前用户信息")
    @GetMapping("/getUserBySession")
    public ResponseBean<User> getUserBySession(HttpServletRequest request) {
        SsoUser ssoUser = SessionUtils.getUser(request);
        //没有登录过
        if (ssoUser == null) {
            return ResponseBean.build(ResultCode.NO_LOGIN);
        }
        User user = userService.getUserByKey(ssoUser.getUserKey());
        return ResponseBean.build(ResultCode.SUCCESS, user);
    }

    @ApiOperation("获取指定用户信息")
    @GetMapping("/getUserByKey")
    public ResponseBean<User> getUserByKey(HttpServletRequest request, @RequestParam("user_key") String userKey) {
        User user = userService.getUserByKey(userKey);
        return ResponseBean.build(ResultCode.SUCCESS, user);
    }

}
