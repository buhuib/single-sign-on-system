package rpc;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import bhb.model.SsoUser;

import java.io.Serializable;

/**
 * 存于session中的用户凭证 根据accessToken来获取
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RpcAccessToken implements Serializable {

    //序列化唯一id
    private static final long serialVersionUID = 4507869346123296527L;

    //登录用户信息凭证
    private String accessToken;


    //需要过多少时间过期
    public int expiresIn;

    //刷新凭证
    private String refreshToken;

    //登录的用户信息
    private SsoUser user;




}
