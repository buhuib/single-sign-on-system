package session;

import bhb.model.SsoUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import rpc.RpcAccessToken;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SessionAccessToken extends RpcAccessToken {

    private static final long serialVersionUID = 4507869346123296527L;

    //过期时间
    private Long expirationTime;

    //判断凭证是否过期
    public boolean isExpired() {
        return System.currentTimeMillis() > expirationTime;
    }

    public SessionAccessToken(String accessToken, int expiresIn, String refreshToken, SsoUser user,
                              long expirationTime) {
        super(accessToken, expiresIn, refreshToken, user);
        this.expirationTime = expirationTime;
    }
}
