package session.redis;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.session.SessionRepository;
import session.SessionMappingStorage;

import javax.servlet.http.HttpSession;

public class RedisSessionMappingStorage implements SessionMappingStorage {
    private static final String SESSION_TOKEN_KEY = "session_token_key_";
    private static final String TOKEN_SESSION_KEY = "token_session_key_";

    private SessionRepository<?> sessionRepository;

    private StringRedisTemplate redisTemplate;

    @Override
    public synchronized void addSessionById(final String accessToken, final HttpSession session) {
        redisTemplate.opsForValue().set(SESSION_TOKEN_KEY + session.getId(), accessToken);

        redisTemplate.opsForValue().set(TOKEN_SESSION_KEY + accessToken, session.getId());
    }

    @Override
    public synchronized void removeBySessionById(final String sessionId) {
        ValueOperations valueOperations = redisTemplate.opsForValue();

        final String accessToken = (String) valueOperations.get(SESSION_TOKEN_KEY + sessionId);
        if (accessToken != null) {
            redisTemplate.delete(TOKEN_SESSION_KEY + accessToken);
            redisTemplate.delete(SESSION_TOKEN_KEY + sessionId);

            sessionRepository.deleteById(sessionId);
        }
    }

    @Override
    public synchronized HttpSession removeSessionByMappingId(final String accessToken) {
        final String sessionId = redisTemplate.opsForValue().get(TOKEN_SESSION_KEY + accessToken);
        if (sessionId != null) {
            removeBySessionById(sessionId);
        }
        return null;
    }

    public SessionRepository<?> getSessionRepository() {
        return sessionRepository;
    }

    public void setSessionRepository(SessionRepository<?> sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    public StringRedisTemplate getRedisTemplate() {
        return redisTemplate;
    }

    public void setRedisTemplate(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }
}
