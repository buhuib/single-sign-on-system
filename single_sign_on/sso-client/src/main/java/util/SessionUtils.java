package util;

import constant.SsoConstant;
import bhb.model.SsoUser;
import rpc.RpcAccessToken;
import session.SessionAccessToken;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * 管理Session的工具类
 */
public class SessionUtils {

    public static SsoUser getUser(HttpServletRequest request) {
        //用Optional来验证token中是否有值 如果没有就直接返回null
        return Optional.ofNullable(getAccessToken(request)).map(token -> token.getUser()).orElse(null);
    }

    //在Session中获取AccessToken的值
    public static SessionAccessToken getAccessToken(HttpServletRequest request) {
        return (SessionAccessToken) request.getSession().getAttribute(SsoConstant.SESSION_ACCESS_TOKEN);
    }

    public static void setAccessToken(HttpServletRequest request, RpcAccessToken rpcAccessToken) {
        SessionAccessToken sessionAccessToken = null;
        if (rpcAccessToken != null) {
            sessionAccessToken = createSessionAccessToken(rpcAccessToken);
        }
        request.getSession().setAttribute(SsoConstant.SESSION_ACCESS_TOKEN, sessionAccessToken);
    }

    private static SessionAccessToken createSessionAccessToken(RpcAccessToken accessToken) {
        long expirationTime = System.currentTimeMillis() + accessToken.getExpiresIn() * 1000;
        return new SessionAccessToken(accessToken.getAccessToken(), accessToken.getExpiresIn(),
                accessToken.getRefreshToken(), accessToken.getUser(), expirationTime);
    }
}
