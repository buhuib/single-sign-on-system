package filter;

import bhb.utils.LogUtils;
import constant.SsoConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LogoutFilter extends ClientFilter {

    @Override
    public boolean isAccessAllowed(HttpServletRequest request, HttpServletResponse response) throws IOException {
        LogUtils.logToFileSimple("登出拦截");

        String accessToken = getLogoutParam(request);
        LogUtils.logToFileSimple("accessToken" + accessToken);
        if (accessToken != null) {
            destroySession(accessToken);
            LogUtils.logToFileSimple("false");
            return false;
        }
        LogUtils.logToFileSimple("true");

        return true;
    }

    protected String getLogoutParam(HttpServletRequest request) {
        return request.getHeader(SsoConstant.LOGOUT_PARAMETER_NAME);
    }

    private void destroySession(String accessToken) {
        final HttpSession session = getSessionMappingStorage().removeSessionByMappingId(accessToken);
        if (session != null) {
            session.invalidate();
        }
    }
}
