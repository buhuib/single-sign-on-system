package filter;

import bhb.model.ResponseBean;
import bhb.model.ResultCode;
import bhb.utils.LogUtils;
import com.alibaba.fastjson.JSON;
import constant.Oauth2Constant;
import constant.SsoConstant;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import rpc.Result;
import rpc.RpcAccessToken;
import session.SessionAccessToken;
import util.Oauth2Utils;
import util.SessionUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class LoginFilter extends ClientFilter {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public boolean isAccessAllowed(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //先获取本地Session是否存在accessToken
        SessionAccessToken sessionAccessToken = SessionUtils.getAccessToken(request);
        //没过期并且更新成功 直接放行
        if (sessionAccessToken != null && (!sessionAccessToken.isExpired() || refreshToken(sessionAccessToken.getRefreshToken(), request))) {
            return true;
        }
        //如果本地session没有 那就在header中查找code 或者在cookie中找
        String code = request.getParameter(Oauth2Constant.AUTH_CODE);
        if (code == null) {
            //cookie中没有 那就在header中找
            code = request.getHeader(Oauth2Constant.AUTH_CODE);
        }
        //找到code之后就去验证
        if (code != null){
            boolean accessToken = getAccessToken(code, request);
            if (!accessToken){
                //验证不通过
                responseJson(response,ResultCode.NO_LOGIN.getCode(), ResultCode.NO_LOGIN.getMsg());
            }
            //验证通过就放行
            return accessToken;
        }

        // 没有code，本地session中也没有，请求sso系统，让sso系统判断改请求是否已经登录过其他系统
        String ssoLoginCode = checkLogin(request, response);

        // 如果还是没有code，证明真的是没有登录
        if (ssoLoginCode != null) {
            // 获取accessToken
            boolean getAccessTokenResult = getAccessToken(ssoLoginCode, request);
            if (!getAccessTokenResult){
                responseJson(response, ResultCode.NO_LOGIN.getCode(), ResultCode.NO_LOGIN.getMsg());
            }
            return getAccessTokenResult;
        }
        responseJson(response, ResultCode.NO_LOGIN.getCode(), ResultCode.NO_LOGIN.getMsg());

        return false;
    }

    //到验证服务器中请求 sso/ssoLogin接口 去获取tgt
    //这个loginPageUrl就是对应yml中的sso.loginPageUrl
    private String checkLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        StringBuilder sbUrl = new StringBuilder(getLoginPageUrl())
                .append("?")
                .append(Oauth2Constant.APP_ID).append("=").append(getAppId());
        if (getRestTemplate() == null){
            return null;
        }
        HttpHeaders headers = new HttpHeaders();
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();      //map里面是请求体的内容
        Cookie[] oldCookies = request.getCookies();
        List<String> cookies =new ArrayList<>();

        if (oldCookies != null){
            for (Cookie cookie:
                    oldCookies) {
                cookies.add(cookie.getName() + "=" +cookie.getValue());
                LogUtils.logToFileSimple(cookie.getName() + "=" +cookie.getValue());
            }
        }
        headers.put(HttpHeaders.COOKIE,cookies);        //将cookie存入头部
        HttpEntity<MultiValueMap<String, String>> reqs = new HttpEntity<MultiValueMap<String, String>>(map, headers);

        ResponseEntity<ResponseBean> resp = getRestTemplate().postForEntity(sbUrl.toString(), reqs, ResponseBean.class);
        ResponseBean respBean = resp.getBody();
        if (respBean != null && ResultCode.SUCCESS.getCode().equals(respBean.getCode())){
            String code = ((LinkedHashMap<String, String>)respBean.getData()).get("code");
            LogUtils.logToFileSimple(respBean.getMsg());
            return code;
        }
        return null;
    }

    /**
     * 获取accessToken和用户信息存session
     *  去验证中心发http请求验证是否存在登录信息
     *  验证中心服务在yml中的sso.url配置  即参数getServerUrl
     *  在SsoSmartConfig中配置注入
     * @param code
     * @param request
     */
    private boolean getAccessToken(String code, HttpServletRequest request) {
        Result<RpcAccessToken> result = Oauth2Utils.getAccessToken(getServerUrl(), getAppId(),
                getAppSecret(), code);
        if (!result.isSuccess()) {
            logger.error("getAccessToken has error, message:{}", result.getMsg());
            return false;
        }
        return setAccessTokenInSession(result.getData(), request);
    }


    /**
     * 通过refreshToken参数调用http请求延长服务端session，并返回新的accessToken
     *
     * @param refreshToken
     * @param request
     * @return
     */
    protected boolean refreshToken(String refreshToken, HttpServletRequest request) {
        Result<RpcAccessToken> result = Oauth2Utils.refreshToken(getServerUrl(), getAppId(), refreshToken);
        if (!result.isSuccess()) {
            logger.error("refreshToken has error, message:{}", result.getMsg());
            return false;
        }
        return setAccessTokenInSession(result.getData(), request);
    }

    //将AccessToken放于Session中
    private boolean setAccessTokenInSession(RpcAccessToken rpcAccessToken, HttpServletRequest request) {
        if (rpcAccessToken == null) {
            return false;
        }
        // 记录accessToken到本地session
        SessionUtils.setAccessToken(request, rpcAccessToken);

        // 记录本地session和accessToken映射
        recordSession(request, rpcAccessToken.getAccessToken());
        return true;
    }
    //注入到session中
    private void recordSession(final HttpServletRequest request, String accessToken) {
        final HttpSession session = request.getSession();
        getSessionMappingStorage().removeBySessionById(session.getId());
        getSessionMappingStorage().addSessionById(accessToken, session);
    }
    //返回错误信息 直接用Writer写出错误信息
    protected void responseJson(HttpServletResponse response, int code, String message) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        response.setStatus(200);
        PrintWriter writer = response.getWriter();
        writer.write(JSON.toJSONString(Result.create(code, message)));
        writer.flush();
        writer.close();
    }

}
