package bhb.user;

import bhb.model.ResponseBean;
import bhb.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "${service-url.cloud-gateway}", contextId = "UserServiceClient")
public interface UserClient {

    @GetMapping("/user/getUserByAccount")
    ResponseBean<User> getUserByAccount(@RequestParam("account") String account);

    @GetMapping("/user/getUserByKey")
    ResponseBean<User> getUserByKey(@RequestParam("user_key") String userKey);

}
