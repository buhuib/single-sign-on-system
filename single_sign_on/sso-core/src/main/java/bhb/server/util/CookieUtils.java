package bhb.server.util;


import org.springframework.util.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookieUtils {

    //私有构造方法不允许创建对象
    private CookieUtils() {
    }

    //根据cookie值获取值
    public static String getCookie(HttpServletRequest request, String name) {
        Cookie[] cookies = request.getCookies();
        //检验是否有cookies与name是否合法
        if (cookies.length == 0 || StringUtils.isEmpty(name)) {
            return null;
        }
        //查找对应的cookie值
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(name)) {
                return cookie.getValue();
            }
        }
        return null;
    }

    //添加cookie
    public static void addCookie(String name, String value, String path, HttpServletRequest request,
                                 HttpServletResponse response) {
        Cookie cookie = new Cookie(name, value);
        if (!StringUtils.isEmpty(path)) {
            //设置该cookie的共享域
            //path为/则为全局共享
            cookie.setPath(path);
        }
        //获取当前请求使用的协议
        if ("https".equals(request.getScheme())) {
            //若为https的时候该cookie才生效
            cookie.setSecure(true);
        }
        //能防止js脚本获取cookie值
        cookie.setHttpOnly(false);
        //添加cookie
        response.addCookie(cookie);
    }

    //删除cookie
    public static void removeCookie(String name, String path, HttpServletResponse response) {
        //新建一个新的值覆盖
        Cookie cookie = new Cookie(name, null);
        if (!StringUtils.isEmpty(path)) {
            cookie.setPath(path);
        }
        //设置最大有效市场 若大于0 则为那个值的最大有效时长
        //若为0则是 当前会话结束后删除cookie
        //若为小于0 则立即删除cookie
        cookie.setMaxAge(-1);
        response.addCookie(cookie);
    }
}

