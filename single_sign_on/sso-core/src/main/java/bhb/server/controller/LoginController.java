package bhb.server.controller;

import bhb.model.LoginResult;
import bhb.model.ResponseBean;
import bhb.model.ResultCode;
import bhb.model.SsoUser;
import bhb.server.service.AppService;
import bhb.server.service.SsoUserService;
import bhb.server.session.CodeManager;
import bhb.server.session.SessionManager;
import constant.Oauth2Constant;
import constant.SsoConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import rpc.Result;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/sso")
public class LoginController {

    @Autowired
    private AppService appService;

    @Autowired
    private SsoUserService ssoUserService;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private CodeManager codeManager;

    @GetMapping("/loginGet")
    public ResponseBean loginGet(
            @RequestParam(value = SsoConstant.REDIRECT_URI, required = false) String redirectUri,
            @RequestParam(value = Oauth2Constant.APP_ID, required = true) String appId,
            @RequestParam String account,
            @RequestParam String password,
            HttpServletRequest request, HttpServletResponse response) {

        //验证是否为非法引用
        if (!appService.exist(appId)) {
            return ResponseBean.build(ResultCode.APP_DISABLED);
        }
        //验证帐号密码的正确性
        Result<SsoUser> loginResult = ssoUserService.login(account, password);
        //如果密码不正确
        if (!loginResult.isSuccess()) {
            request.setAttribute("errorMessage", loginResult.getMsg());
            return ResponseBean.build(ResultCode.ERROR, loginResult.getMsg());
        }
        //创建全局的tgt-userInfo缓存 并且存储在认证中心的cookie
        String tgt = sessionManager.setUser(loginResult.getData(), response, request);
        //放一个一次性使用的code在Session中
        String code = codeManager.generate(tgt, true, redirectUri);
        return ResponseBean.build(ResultCode.SUCCESS, new LoginResult(code));
    }

}
