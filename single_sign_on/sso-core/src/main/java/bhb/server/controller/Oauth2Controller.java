package bhb.server.controller;

import bhb.model.AccessTokenContent;
import bhb.model.SsoUser;
import bhb.model.CodeContent;
import bhb.server.common.RefreshTokenContent;
import bhb.server.service.AppService;
import bhb.server.service.SsoUserService;
import bhb.server.session.AccessTokenManager;
import bhb.server.session.CodeManager;
import bhb.server.session.RefreshTokenManager;
import bhb.server.session.TicketGrantingTicketManager;
import constant.Oauth2Constant;
import enums.GrantTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import rpc.Result;
import rpc.RpcAccessToken;

@RestController
@RequestMapping("/oauth2")
public class Oauth2Controller {

    @Autowired
    private AppService appService;

    @Autowired
    private CodeManager codeManager;

    @Autowired
    private TicketGrantingTicketManager ticketGrantingTicketManager;

    @Autowired
    private SsoUserService ssoUserService;

    @Autowired
    private RefreshTokenManager refreshTokenManager;

    @Autowired
    private AccessTokenManager accessTokenManager;

    //远程系统校验请求是否登录
    @GetMapping("/access_token")
    public Result getAccessToken(
            @RequestParam(value = Oauth2Constant.GRANT_TYPE, required = true) String grantType,
            @RequestParam(value = Oauth2Constant.APP_ID, required = true) String appId,
            @RequestParam(value = Oauth2Constant.APP_SECRET, required = true) String appSecret,
            @RequestParam(value = Oauth2Constant.AUTH_CODE, required = false) String code,
            @RequestParam(value = Oauth2Constant.USERNAME, required = false) String username,
            @RequestParam(value = Oauth2Constant.PASSWORD, required = false) String password) {

        Result<Void> checkParam = validateParam(grantType, code, username, password);
        //参数校验不合法
        if (!checkParam.isSuccess()) {
            return checkParam;
        }
        //校验是否合法应用
        Result<Void> validate = appService.validate(appId, appSecret);
        if (!validate.isSuccess()) {
            return validate;
        }
        //去验证当前code是否有登录信息
        Result<AccessTokenContent> result = validateAuth(grantType, code, username, password, appId);
        if (!result.isSuccess()) {
            //不成功就是没有登录过
            return result;
        }
        //返回AccessToken登录凭证
        return Result.createSuccess(genereateRpcAccessToken(result.getData(), null));
    }


    /**
     * 刷新accessToken，并延长TGT超时时间
     *
     * accessToken刷新结果有两种：
     * 1. 若accessToken已超时，那么进行refreshToken会生成一个新的accessToken，新的超时时间；
     * 2. 若accessToken未超时，那么进行refreshToken不会改变accessToken，但超时时间会刷新，相当于续期accessToken。
     *
     * @param appId
     * @param refreshToken
     * @return
     */
    @RequestMapping(value = "/refresh_token", method = RequestMethod.GET)
    public Result refreshToken(
            @RequestParam(value = Oauth2Constant.APP_ID, required = true) String appId,
            @RequestParam(value = Oauth2Constant.REFRESH_TOKEN, required = true) String refreshToken) {
        //检验是否合法应用
        if(!appService.exist(appId)) {
            return Result.createError("非法应用");
        }
        //查看目标token是否存在 若不存在说明已经超时
        RefreshTokenContent refreshTokenContent = refreshTokenManager.validate(refreshToken);
        if (refreshTokenContent == null) {
            return Result.createError("refreshToken有误或已过期");
        }
        AccessTokenContent accessTokenContent = refreshTokenContent.getAccessTokenContent();
        if (!appId.equals(accessTokenContent.getAppId())) {
            return Result.createError("非法应用");
        }
        SsoUser user = ticketGrantingTicketManager.getAndRefresh(accessTokenContent.getCodeContent().getTgt());
        if (user == null) {
            return Result.createError("服务端session已过期");
        }
        //续期token
        return Result.createSuccess(genereateRpcAccessToken(accessTokenContent, refreshTokenContent.getAccessToken()));
    }


    //校验请求登录参数是否合法
    private Result<Void> validateParam(String grantType, String code, String username, String password) {
        if (GrantTypeEnum.AUTHORIZATION_CODE.getValue().equals(grantType)) {
            if (StringUtils.isEmpty(code)) {
                return Result.createError("code不能为空");
            }
        } else if (GrantTypeEnum.PASSWORD.getValue().equals(grantType)) {
            if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
                return Result.createError("username和password不能为空");
            }
        } else {
            return Result.createError("授权方式不支持");
        }
        return Result.success();
    }

    //校验是否已经存在登录信息
    private Result<AccessTokenContent> validateAuth(String grantType, String code, String username, String password,
                                                    String appId) {
        AccessTokenContent authDto = null;
        //通过按照code来验证登录
        if (GrantTypeEnum.AUTHORIZATION_CODE.getValue().equals(grantType)) {
            //获取code来登录 获取完直接删除redis中的信息
            CodeContent codeContent = codeManager.getAndRemove(code);
            if (codeContent == null) {
                return Result.createError("code有误或已过期");
            }
            //直接根据code信息创建tgt-userInfo
            SsoUser user = ticketGrantingTicketManager.getAndRefresh(codeContent.getTgt());
            if (user == null) {
                return Result.createError("服务端session已过期");
            }
            authDto = new AccessTokenContent(codeContent, user, appId);
        }
        //直接登录模式
        else if (GrantTypeEnum.PASSWORD.getValue().equals(grantType)) {
            // app通过此方式由客户端代理转发http请求到服务端获取accessToken
            Result<SsoUser> loginResult = ssoUserService.login(username, password);
            if (!loginResult.isSuccess()) {
                return Result.createError(loginResult.getMsg());
            }
            SsoUser user = loginResult.getData();
            String tgt = ticketGrantingTicketManager.generate(loginResult.getData());
            CodeContent codeContent = new CodeContent(tgt, false, null);
            authDto = new AccessTokenContent(codeContent, user, appId);
        }
        return Result.createSuccess(authDto);
    }

    private RpcAccessToken genereateRpcAccessToken(AccessTokenContent accessTokenContent, String accessToken) {
        String newAccessToken = accessToken;
        if (newAccessToken == null || !accessTokenManager.refresh(newAccessToken)) {
            newAccessToken = accessTokenManager.generate(accessTokenContent);
        }
        String refreshToken = refreshTokenManager.generate(accessTokenContent, newAccessToken);

        return new RpcAccessToken(newAccessToken, accessTokenManager.getExpiresIn(), refreshToken,
                accessTokenContent.getUser());
    }

}
