package bhb.server.service.impl;

import bhb.model.ResponseBean;
import bhb.model.ResultCode;
import bhb.model.SsoUser;
import bhb.model.User;
import bhb.server.service.SsoUserService;
import bhb.user.UserClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rpc.Result;

@Service
public class SsoUserServiceImpl implements SsoUserService {

    @Autowired
    private UserClient userClient;

    @Override
    public Result<SsoUser> login(String account, String password) {
        //查找用户信息
        ResponseBean<User> userInfo = userClient.getUserByAccount(account);
        //如果rpc调用出错 则生成错误信息
        if (!userInfo.getCode().equals(ResultCode.SUCCESS.getCode())) {
            return Result.createError(userInfo.getMsg());
        }
        User user = userInfo.getData();
        //查看查询返回的结果
        if (user == null) {
            return Result.createError("用户不存在");
        }
        //检验用户的密码是否正确
        if (user.getPassword().equals(password)) {
            SsoUser data = new SsoUser();
            data.setAccount(user.getAccount()).setUserKey(user.getUserKey())
                    .setNickname(user.getNickname()).setCompKey(user.getCompKey()).setUid(user.getUid());
            return Result.createSuccess(data);
        }
        return Result.createError("密码不正确");
    }

}
