package bhb.server.service;

import bhb.model.SsoUser;
import rpc.Result;

public interface SsoUserService {

    public Result<SsoUser> login(String account, String password);

}
