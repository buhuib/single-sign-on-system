package bhb.server.service;

import bhb.server.model.App;
import bhb.model.ResponseBean;
import rpc.Result;

import java.util.List;

public interface AppService {

    public boolean exist(String appId);

    List<App> getAppList();

    Result<Void> validate(String appId, String appSecret);

}
