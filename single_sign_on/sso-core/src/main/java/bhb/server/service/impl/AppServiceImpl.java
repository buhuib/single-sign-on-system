package bhb.server.service.impl;

import bhb.server.model.App;
import bhb.model.ResponseBean;
import bhb.model.ResultCode;
import bhb.server.service.AppService;
import org.springframework.stereotype.Service;
import rpc.Result;

import java.util.LinkedList;
import java.util.List;

@Service
public class AppServiceImpl implements AppService {

    private static List<App> appList;

    static {
        appList = new LinkedList<>();

        appList.add(new App("单点登录系统", "sso-login-server", "88bea2c89de69aac2bfa5a14a86a3810"));
        appList.add(new App("单点登录系统", "sso-login-server-local", "88bea2c89de69aac2bfa5a14a86a3810"));

        appList.add(new App("用户服务", "sso-user-server", "33129ac0e0411e0aabdb207317d67c4e"));
        appList.add(new App("用户服务", "sso-user-server-local", "33129ac0e0411e0aabdb207317d67c4e"));

//        appList.add(new App("仓库服务", "cloud-warehouse-server", "2a2de238ab5d0a4a22d65aa6e460b0cd"));
//        appList.add(new App("仓库服务", "cloud-warehouse-server-local", "2a2de238ab5d0a4a22d65aa6e460b0cd"));

    }

    //匹配应用唯一标识是否存在
    @Override
    public boolean exist(String appId) {
        return appList.stream().anyMatch(app -> app.getAppId().equals(appId));
    }

    @Override
    public List<App> getAppList() {
        return this.getAppList();
    }

    @Override
    public Result<Void> validate(String appId, String appSecret) {
        for (App app : appList) {
            if (app.getAppId().equals(appId)) {
                if (app.getAppSecret().equals(appSecret)) {
                    return Result.success();
                } else {
                    return Result.createError("appSecret有误");
                }
            }
        }
        return Result.createError("appId不存在");
    }
}
