package bhb.server.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class App {

    //应用独特密钥
    private String appSecret;

    //应用唯一标识
    private String appId;

    //应用名称
    private String name;

}
