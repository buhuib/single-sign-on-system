package bhb.server.session;

import bhb.server.common.Expiration;
import bhb.model.SsoUser;

import java.util.UUID;

//生成tgt - userInfo 存于redis中
public interface TicketGrantingTicketManager extends Expiration {

    default String generate(SsoUser userInfo) {
        //生成一个随机的TGT
        String tgt = "TGT-" + UUID.randomUUID().toString().replace("-", "");
        create(tgt, userInfo);
        return tgt;
    }


    //生成TGT-userInfo的键值对 存放在Session中
    void create(String tgt, SsoUser userInfo);

    //获取tgt对应的登录信息并且刷新对应的过期时间
    SsoUser getAndRefresh(String tgt);

    //更新对应的tgt登录信息
    void update(String tgt,SsoUser userInfo);

    //删除对应的tgt登录信息
    void remove(String tgt);

}
