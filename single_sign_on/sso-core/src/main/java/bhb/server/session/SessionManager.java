package bhb.server.session;

import bhb.server.constant.AppConstant;
import bhb.model.SsoUser;
import bhb.server.util.CookieUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class SessionManager {

    @Autowired
    private TicketGrantingTicketManager ticketGrantingTicketManager;

    public String setUser(SsoUser userInfo, HttpServletResponse response, HttpServletRequest request) {
        String tgt = getCookieTgt(request);
        if (StringUtils.isEmpty(tgt)) {
            //没有登陆过
            tgt = ticketGrantingTicketManager.generate(userInfo);
            //放置于cookie中
            CookieUtils.addCookie(AppConstant.TGC, tgt, "/", request, response);
        } else if (ticketGrantingTicketManager.getAndRefresh(tgt) == null) {
            //cookie虽然已经有tgt 即以前已经登陆过 但是登录已经过期
            //需要重新创建tgt-userInfo键值对
            ticketGrantingTicketManager.create(tgt, userInfo);
        } else {
            //正常 但可能重新登录了 需要update用户信息
            ticketGrantingTicketManager.update(tgt, userInfo);
        }
        return tgt;
    }

    private String getCookieTgt(HttpServletRequest request) {
        return CookieUtils.getCookie(request, AppConstant.TGC);
    }
}
