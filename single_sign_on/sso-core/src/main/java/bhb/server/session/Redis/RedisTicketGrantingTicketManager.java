package bhb.server.session.Redis;

import bhb.model.SsoUser;
import bhb.server.session.TicketGrantingTicketManager;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.concurrent.TimeUnit;

@Component
public class RedisTicketGrantingTicketManager implements TicketGrantingTicketManager {

    @Value("${sso.timeout}")
    private int timeout;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public int getExpiresIn() {
        return timeout;
    }

    @Override
    public void create(String tgt, SsoUser userInfo) {
        //设置对应tgt
        redisTemplate.opsForValue().set(tgt, JSON.toJSONString(userInfo), getExpiresIn(), TimeUnit.SECONDS);
    }

    @Override
    public SsoUser getAndRefresh(String tgt) {
        //先尝试获得对应的用户信息
        String userInfo = redisTemplate.opsForValue().get(tgt);
        //没登录过或者登录过期
        if (StringUtils.isEmpty(userInfo)) {
            return null;
        }
        //更新过期时间
        redisTemplate.expire(tgt, timeout, TimeUnit.SECONDS);
        //返回序列化对象
        return JSONObject.parseObject(userInfo, SsoUser.class);
    }

    @Override
    public void update(String tgt, SsoUser userInfo) {
        //重新新增 覆盖原有key-value
        create(tgt, userInfo);
    }

    @Override
    public void remove(String tgt) {
        //直接删除key
        redisTemplate.delete(tgt);
    }
}
