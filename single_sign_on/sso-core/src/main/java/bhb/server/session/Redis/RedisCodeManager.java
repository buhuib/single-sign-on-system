package bhb.server.session.Redis;

import bhb.model.CodeContent;
import bhb.server.session.CodeManager;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.concurrent.TimeUnit;

@Component
public class RedisCodeManager implements CodeManager {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public void create(String code, CodeContent content) {
        //将一次性的code信息放于redis中
        redisTemplate.opsForValue().set(code, JSON.toJSONString(content), getExpiresIn(), TimeUnit.SECONDS);
    }

    @Override
    public CodeContent getAndRemove(String code) {
        String codeContent = redisTemplate.opsForValue().get(code);
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        redisTemplate.delete(code);
        return JSONObject.parseObject(codeContent, CodeContent.class);
    }
}
