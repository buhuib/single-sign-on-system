package bhb.server.session;

import bhb.model.CodeContent;
import bhb.server.common.Expiration;

import java.util.UUID;

public interface CodeManager extends Expiration {

    default String generate(String tgt, boolean sendLogoutRequest, String redirectUri) {
        String code = "code-" + UUID.randomUUID().toString().replace("-", "");
        create(code, new CodeContent(tgt, sendLogoutRequest, redirectUri));
        return code;
    }

    //
    void create(String code, CodeContent content);

    //到目标服务器注册的时候直接获取然后删除
    CodeContent getAndRemove(String code);

    //写死默认过期10min
    @Override
    default int getExpiresIn() {
        return 600;
    }
}
