package bhb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccessTokenContent {

    private static final long serialVersionUID = 4587667812642196058L;

    private CodeContent codeContent;
    private SsoUser user;
    private String appId;

}
