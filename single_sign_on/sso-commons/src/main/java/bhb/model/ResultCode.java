package bhb.model;

public enum ResultCode {
    SUCCESS(10000,"成功"),

    PARAM_EXCEPTION(20000,"参数异常"),

    MYSQL_WRITE_FAIL(30000,"数据库写入失败"),

    ERROR(40000,"未知失败,请联系管理员！"),
    APP_DISABLED(40001,"非法应用"),
    NO_LOGIN(40002,"未登录或登录过期"),
    SEND_LOGIN_SMS_FAIL(40003,"验证码发送失败"),
    GOODS_NOT_EXIST(40004,"物品不存在"),
    VERIFICATION_CODE_ERROR(40005,"验证码错误"),
    LOGOUT_FAIL(40006,"退出登录失败"),
    SECRET_WRONG(40007,"appSecret有误"),

    INVITATION_TICKET(50000, "邀请码无效或已过期"),
    USER_NOT_EXIST(50001, "用户不存在"),


    SAVE_WAREHOUSE_FAILED(60001,"新增仓库失败"),
    SAVE_WAREHOUSE_PRINCIPAL_FAILED(60002,"仓库负责人信息保存失败"),
    SAVE_WAREHOUSE_ASSOCIATE_FAILED(60003,"仓库等级信息保存失败"),
    SAVE_WAREHOUSE_STOCK_FAILED(60004,"仓库物品信息保存失败"),
    GOODS_LIST_ISEMPTY(60005,"仓库物品信息为空，请检查！"),
    QUERY_WAREHOUSE_ISEMPTY(60006,"查询仓库信息为空！！"),
    QUERY_WAREHOUSE_ASSOCIATE_ISEMPTY(60007,"查询仓库关系信息为空！！"),
    QUERY_WAREHOUSE_STOCK_ISEMPTY(60008,"查询仓库物品信息为空！！"),
    QUERY_WAREHOUSE_PRINCIPAL_ISEMPTY(60009,"查询仓库负责人信息为空！！"),
    WAREHOUSE_GOODS_IS_NOT_EMPTY(60010,"仓库库存不为空，删除失败!!"),
    WAREHOUSE_DELETE_FAILED(60011,"仓库信息删除失败！！"),
    WAREHOUSE_ASSOCIATE_DELETE_FAILED(60012,"仓库关联表信息删除失败，仓库删除失败!!"),
    WAREHOUSE_PRINCIPAL_DELETE_FAILED(60013,"仓库负责人信息删除失败，仓库删除失败!!"),
    WAREHOUSE_UPDATE_FAILED(60013,"更新仓库信息失败!!"),
    REMOTE_CALL_UNIT_CONVERSION_FAILED(60014,"远程获取单位转换比例失败!!"),
    REMOTE_CALL_UNIT_NAMES_FAILED(60015,"远程获取单位名称比例失败!!"),
    REMOTE_CALL_GOODS_NAMES_FAILED(60016,"远程获取物品名称比例失败!!"),



    ORDER_UPLOAD_EXCEL_EMPTY(70000, "文件为空"),
    BUILD_STATUS_ORDER_NOT_UNIQUE(70001, "文档中对应多个主订单"),
    RPC_LIST_GOODS_FAIL(70002, "远程获取物品失败"),
    STORE_UPDATE_FAILED(70003,"门店名称已经存在,请换名称重试！"),
    STORE_IS_EXIST(70004,"门店已经存在，新增失败！"),
    STORE_ADD_FAILED(70005,"门店新增失败！"),
    ORDER_NOT_EXIST(70006, "订单不存在"),
    STORE_KEY_ISEMPTY(70007,"门店key不能为空，请检查！"),
    STORE_NOT_EXIST(70008,"门店不存在！！"),
    KEY_PARAMETER_IS_EMPTY(70009,"关键参数不能为空，请检查！"),
    STORE_ABNORMAL_ADD_FAILED(70010,"门店异常新增失败！"),
    STORE_ABNORMAL_UPDATE_FAILED(70011,"门店异更新失败！"),
    ABNORMAL_HANDLE_ADD_FAILED(70012,"门店异常处理失败！"),
    FILE_UPLOAD_TO_FS_FAIL(70013,"源文件上传到服务器失败！"),
    ORDER_GOODS_NOT_EXIST(70014,"订单物品不存在！"),
    EXCEL_UPLOAD_TO_ORDER_FAIL(70015,"excel解析失败！"),
    ORDER_NOT_REGION(70016,"订单没有找到对应区域！"),
    NO_ORDER_OPTION(70017 ,"没有订单操作！"),
    CHECK_ORDER_FAIL(70018,"检查门店订单失败"),
    STORE_HAS_ORDER(70019,"门店下面还有订单，删除失败！"),



    TYPE_NOT_EXIST(80000,"自定义类型不存在"),
    CONFIG_NOT_EXIST(80001,"自定义配置不存在"),
    CONTENT_EXIST(80002,"当前内容已存在"),

    TRANSPORT_NOT_EXIST(90000,"运输记录不存在"),

    PURCHASE_NOT_EXIST(10000,"采购不存在"),
    ;
    private Integer code;
    private String msg;
    private ResultCode(Integer code, String msg) {
        this.code = code;
        this.msg= msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


}

