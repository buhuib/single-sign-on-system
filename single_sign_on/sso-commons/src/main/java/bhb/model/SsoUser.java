package bhb.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 已登录用户信息
 * 
 * @author Joe
 */
@ApiModel
@Data
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SsoUser implements Serializable {

	private static final long serialVersionUID = 1764365572138947234L;

	// 登录成功userId
    @ApiModelProperty("用户id")
    private Integer uid;
    // 登录名
    @ApiModelProperty("用户名称")
    private String nickname;
    // 账号
    @ApiModelProperty("用户账号")
    private String account;
    // 用户key
    @ApiModelProperty("用户key")
    @JsonProperty("user_key")
    private String userKey;
    // 公司key
    @ApiModelProperty(value = "公司key")
    @JsonProperty("comp_key")
    private String compKey;

    public SsoUser(){

    }
    public SsoUser(Integer id, String username) {
        super();
        this.uid = id;
        this.nickname = username;
    }



    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SsoUser other = (SsoUser)obj;
        if (userKey == null) {
            if (other.userKey != null)
                return false;
        } else if (!userKey.equals(other.userKey))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "SsoUser{" +
                "uid=" + uid +
                ", nickname='" + nickname + '\'' +
                ", account='" + account + '\'' +
                ", userKey='" + userKey + '\'' +
                ", compKey='" + compKey + '\'' +
                '}';
    }
}
