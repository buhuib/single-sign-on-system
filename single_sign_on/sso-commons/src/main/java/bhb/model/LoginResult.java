package bhb.model;

import lombok.Data;

@Data
public class LoginResult {

    String code;

    public LoginResult(String code) {
        this.code = code;
    }

}
