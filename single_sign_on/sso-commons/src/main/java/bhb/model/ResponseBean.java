package bhb.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ResponseBean<T> {

    @ApiModelProperty(value = "接口状态码", required = true)
    private Integer code;
    @ApiModelProperty(value = "接口状态信息", required = true)
    private String msg;
    @ApiModelProperty(value = "具体数据", required = true)
    private T data;

    public static <T> ResponseBean<T> build(ResultCode rc) {
        return new ResponseBean<T>(rc.getCode(), rc.getMsg());
    }

    public static <T> ResponseBean<T> build(ResultCode rc, T t) {
        return new ResponseBean<T>(rc.getCode(), rc.getMsg(), t);
    }

    public static <T> ResponseBean<T> build(ResultCode rc, String msg) {
        return new ResponseBean<T>(rc.getCode(), msg);
    }

    public static <T> ResponseBean<T> build(Integer code, String msg, T t) {
        return new ResponseBean<T>(code, msg, t);
    }

    public static <T> ResponseBean<T> build(Integer code, String msg) {
        return new ResponseBean<T>(code, msg);
    }

    private ResponseBean() {
    }

    private ResponseBean(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    private ResponseBean(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}