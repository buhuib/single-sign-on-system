package bhb.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@AllArgsConstructor
@ToString
@ApiModel
// 转成json时候，驼峰转下划线
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class User {

    public static Integer STATUS_INVITATION = 0;        // 邀请中
    public static Integer STATUS_NORMAL_NO_BINDING = 1; // 正常-未绑定
    public static Integer STATUS_NORMAL_BINDING = 2;    // 正常-已绑定

    @ApiModelProperty("用户id")
    private Integer uid;
    @ApiModelProperty("公司key")
    private String  compKey;
    @ApiModelProperty("用户key")
    private String  userKey;
    @ApiModelProperty("账号")
    private String  account;
    @ApiModelProperty("密码")
    private String  password;
    @ApiModelProperty("名称")
    private String  nickname;
    @ApiModelProperty("微信openid")
    private String  openid;
    @ApiModelProperty("最后一次登录ip")
    private String  lastLoginIp;
    @ApiModelProperty("最后一次登录时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastLoginTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("创建时间")
    private Date    gmtCreate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("修改时间")
    private Date    gmtModified;
    @ApiModelProperty("是否删除")
    private Integer isDelete;
    @ApiModelProperty("角色")
    private String roles;
    @ApiModelProperty("状态")
    private Integer  status;

    @ApiModelProperty(hidden = true)
    private Integer count;

    public User(){

    }

    public User(String userKey, String compKey){
        this.userKey = userKey;
        this.compKey = compKey;
    }

    public User(Integer uid, String userKey, String compKey){
        this.uid = uid;
        this.userKey = userKey;
        this.compKey = compKey;
    }

}