package bhb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CodeContent implements Serializable {


    private static final long serialVersionUID = -1332598459045608781L;

    private String tgt;
    private boolean sendLogoutRequest;
    private String redirectUri;
}
